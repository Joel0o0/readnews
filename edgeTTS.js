import { speak, fetchEdgeVoices } from './edgeTTSModule.js'; // Assuming the module is named edgeTTSModule.js

// HTML转义函数
window.escapeHtml = function (text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}

// 使用文件名作为唯一标识符
function getFileIdentifier(file) {
    return file.name;
}

// 保存阅读进度
function saveProgress(file, index) {
    const identifier = getFileIdentifier(file);
    localStorage.setItem(identifier, index);
}

// 获取保存的阅读进度
window.getSavedProgress = function (file, sectionsLength) {
    const identifier = getFileIdentifier(file);
    const savedIndex = parseInt(localStorage.getItem(identifier)) || 0;
    // 确保返回的索引不超过 sections 的长度
    return Math.min(savedIndex, sectionsLength - 1);
}

window.scrollToCurrentSection = function (index) {
    const articleContent = document.getElementById('article-content');
    if (!articleContent) return;

    const sectionElement = articleContent.querySelector(`[data-section="${index}"]`);
    if (sectionElement) {
        sectionElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
}

const DEFAULT_SPEED = '1.4';


var textToSpeak = document.getElementById('textToSpeak');
if (!textToSpeak) {
    textToSpeak = document.getElementById('article-content');
}
const voiceSelect = document.getElementById('voiceSelect');
const speakButton = document.getElementById('speakButton');
const replayButton = document.getElementById('replayButton');
const pauseButton = document.getElementById('pauseButton');
const resumeButton = document.getElementById('resumeButton');
const speedSelect = document.getElementById('speedSelect');

const defaultVoice = 'Microsoft Server Speech Text to Speech Voice (zh-CN, XiaoxiaoNeural)';
let isPlaying = false;
let pausedAt = 0;
let audioElement = null;

try {
    const voices = await fetchEdgeVoices();
    voices.forEach(voice => {
        const option = document.createElement('option');
        option.value = voice.voiceURI;
        option.textContent = `${voice.name} (${voice.lang})`;
        voiceSelect.appendChild(option);
    });

    // Set default voice if exists
    const defaultVoiceOption = Array.from(voiceSelect.options).find(option => option.value === defaultVoice);
    if (defaultVoiceOption) {
        voiceSelect.value = defaultVoiceOption.value;
    }
} catch (error) {
    console.error('Error fetching voices:', error);
}

const setupMediaSession = () => {
    if ('mediaSession' in navigator) {
        navigator.mediaSession.setActionHandler('play', resumeAudio);
        navigator.mediaSession.setActionHandler('pause', pauseAudio);
        navigator.mediaSession.setActionHandler('stop', () => {
            pauseAudio();
            pausedAt = 0;
        });
    }
};

window.sections = [];
window.currentSectionIndex = 0;
window.audioDataCache = [];
let isGenerating = false;

// 优化后的分割函数
window.splitTextIntoSections = function(text, maxLength = 100) {
    const sections = [];
    let currentSection = "";
    
    // 定义需要进行分割的符号
    const splitSymbols = /([。!！?？\n])/g;
    
    // 在需要分割的符号后插入占位符 {SPLIT_HERE}
    const textWithPlaceholder = text.replace(splitSymbols, '$1{SPLIT_HERE}');
    
    // 按照占位符进行分割
    const splitParts = textWithPlaceholder.split('{SPLIT_HERE}');
    
    for (let part of splitParts) {
        // part = part.trim();
        if (part.length === 0) continue;

        // console.log("当前 part:" + part);
        // console.log("当前 part长度" + part.length);
        // console.log("currentSection长度" + currentSection.length);

        // 如果当前段落加上新的部分超过maxLength，则先推送当前段落，再开始新的段落
        if ((currentSection + part).length > maxLength) {
            if (currentSection.length > 0) {
                // console.log("加入新 part 会超长，提前分段");
                sections.push(currentSection);
                currentSection = "";
            }
        }

        // 添加部分到当前段落
        // 保留原有的分割符号
        currentSection += part;

        // 如果当前段落达到了maxLength，推送并重置
        if (currentSection.length >= maxLength) {
            // console.log("当前段落达到了maxLength");
            sections.push(currentSection);
            currentSection = "";
        }
    }

    // 添加剩余的内容
    if (currentSection.length > 0) {
        sections.push(currentSection);
    }

    return sections;
}

function highlightSection(index) {
    const articleContent = document.getElementById('article-content');
    if (!articleContent) {
        return;
    }
    articleContent.innerHTML = sections.map((section, i) =>
        `<span class="${i === index ? 'highlight' : ''}" data-section="${i}">${section}</span>`
    ).join('');

    // 添加点击事件监听器
    articleContent.querySelectorAll('span').forEach(span => {
        span.addEventListener('click', (event) => {
            const clickedSectionIndex = parseInt(event.target.getAttribute('data-section'));
            if (!isNaN(clickedSectionIndex)) {
                if (clickedSectionIndex === currentSectionIndex && isPlaying) {
                    // 如果点击的是当前正在播放的段落，则暂停
                    pauseAudio();
                } else {
                    // 否则，播放点击的段落
                    pauseAudio();
                    currentSectionIndex = clickedSectionIndex;
                    playSection(currentSectionIndex);
                }
            }
        });
    });
}

async function generateAudioForSection(index) {
    if (index >= sections.length || audioDataCache[index]) return;

    isGenerating = true;
    try {
        console.log(`开始生成语音 ${index}`);
        const audioData = await speak({
            text: sections[index],
            voice: voiceSelect.value,
            rate: speedSelect ? speedSelect.value : DEFAULT_SPEED,
            onStartSpeaking: () => console.log(`Generating audio for section ${index}...`),
            onFinish: () => console.log(`Finished generating audio for section ${index}.`)
        });
        audioDataCache[index] = audioData;
    } catch (error) {
        console.error(`Error generating audio for section ${index}:`, error);
    }
    isGenerating = false;

    // 如果当前正在播放，且下一节还没有生成，则开始生成下一节
    if (isPlaying && index === currentSectionIndex && index + 1 < sections.length) {
        generateAudioForSection(index + 1);
    }
}

window.playSection = async function (index) {
    if (index >= sections.length) return;

    highlightSection(index);
    currentSectionIndex = index;

    if (!audioDataCache[index]) {
        await generateAudioForSection(index);
    }

    if (audioElement) {
        audioElement.pause();
    }

    playAudio(audioDataCache[index]);

    const fileInput = document.getElementById('fileInput');
    if (fileInput) {
        // 自动保存进度
        const file = fileInput.files[0];
        if (file) {
            saveProgress(file, index);
        }
    }

    // 开始生成下一节的音频
    if (index + 1 < sections.length && !audioDataCache[index + 1] && !isGenerating) {
        generateAudioForSection(index + 1);
    }
}

const playAudio = (audioData) => {

    // 创建一个 Blob URL 用于音频元素
    const blob = new Blob([audioData], { type: 'audio/mp3' });
    const url = URL.createObjectURL(blob);

    // 如果已存在音频元素，则停止并移除
    if (audioElement) {
        audioElement.pause();
        audioElement.remove();
    }

    // 创建新的音频元素
    audioElement = new Audio(url);
    audioElement.loop = false;
    document.body.appendChild(audioElement);

    // 设置当前时间并播放
    audioElement.currentTime = 0;
    audioElement.play().then(() => {
        isPlaying = true;

        // 更新 MediaSession
        if ('mediaSession' in navigator) {
            navigator.mediaSession.playbackState = 'playing';
        }
    }).catch(e => console.error('Error playing audio:', e));

    audioElement.onended = () => {
        isPlaying = false;
        if ('mediaSession' in navigator) {
            navigator.mediaSession.playbackState = 'none';
        }

        // 播放下一节
        currentSectionIndex++;
        if (currentSectionIndex < sections.length) {
            playSection(currentSectionIndex);
        } else {
            currentSectionIndex = 0;
            highlightSection(-1); // 移除所有高亮
        }
    };
};

const pauseAudio = () => {
    if (audioElement && isPlaying) {
        audioElement.pause();
        pausedAt = audioElement.currentTime;
        isPlaying = false;
        if ('mediaSession' in navigator) {
            navigator.mediaSession.playbackState = 'paused';
        }
    }
};

const resumeAudio = () => {
    if (!isPlaying && audioElement) {
        audioElement.currentTime = pausedAt;
        audioElement.play().then(() => {
            isPlaying = true;
            if ('mediaSession' in navigator) {
                navigator.mediaSession.playbackState = 'playing';
            }
            // 如果下一节还没有生成，开始生成
            if (currentSectionIndex + 1 < sections.length && !audioDataCache[currentSectionIndex + 1] && !isGenerating) {
                generateAudioForSection(currentSectionIndex + 1);
            }
        }).catch(e => console.error('Error resuming audio:', e));
    }
};


const progressText = document.getElementById('progressText');
let progressInterval;

const updateProgress = (start) => {
    let progress = 0;
    progressInterval = setInterval(() => {
        progress += 1;
        if (progress >= 99) {
            clearInterval(progressInterval);
            return;
        }
        progressText.textContent = `生成进度: ${progress}%`;
    }, 100);
};

const completeProgress = () => {
    clearInterval(progressInterval);
    progressText.textContent = '生成进度: 100%';
};

speakButton.addEventListener('click', async () => {
    try {
        var text = textToSpeak.value || textToSpeak.textContent;
        text = escapeHtml(text);
        sections = splitTextIntoSections(text);
        currentSectionIndex = 0;
        audioDataCache = [];
        await playSection(currentSectionIndex);
        setupMediaSession();
    } catch (error) {
        console.error('Error speaking:', error);
    }
});

replayButton.addEventListener('click', () => {
    if (sections.length > 0) {
        currentSectionIndex = 0;
        playSection(currentSectionIndex);
    } else {
        console.log('No audio data to replay. Please generate the audio first.');
    }
});

pauseButton.addEventListener('click', () => {
    pauseAudio();
});

resumeButton.addEventListener('click', () => {
    resumeAudio();
});
setupMediaSession();

const scrollToHighlightButton = document.getElementById('scrollToHighlight');
if (scrollToHighlightButton) {
    scrollToHighlightButton.addEventListener('click', function () {
        scrollToCurrentSection(currentSectionIndex);
    });
}

